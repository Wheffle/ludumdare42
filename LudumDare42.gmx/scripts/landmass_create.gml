/// id = landmass_create(x, y, width, height);

var land = instance_create(argument0, argument1, obj_landmass);
with(land)
{
    width = argument2;
    height = argument3;
    grid = ds_grid_create(width, height);
    
    ds_grid_clear(grid, 0);
}
return land;
