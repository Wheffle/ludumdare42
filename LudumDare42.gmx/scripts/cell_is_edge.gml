/// cell_is_edge(landmass_id, cell_x, cell_y);

var land = argument0;
var cell_x = argument1;
var cell_y = argument2;

var cell_value = landmass_get_cell(land, cell_x, cell_y);
for (var i = -1; i < 2; i++)
{
    for (var j = -1; j< 2; j++)
    {
        if ((i == 0 and j != 0) or (i != 0 and j == 0))
        {
            var neighbor_x = cell_x+i;
            var neighbor_y = cell_y+j;
            var neighbor_value = landmass_get_cell(id, neighbor_x, neighbor_y);
            if (is_undefined(neighbor_value) or !cell_get_attribute(neighbor_value, LAND_ATTR_EXISTS))
            {
                return true;
            }
        }
    }
}

return false;
