/// blueprint_can_place(blueprint_id);

with(argument0)
{
    for (var i = 0; i < cell_width; i++)
    {
        for (var j = 0; j < cell_height; j++)
        {
            var cell_value = landmass_get_cell(land, cell_x+i, cell_y+j);
            if (is_undefined(cell_value) or 
                !cell_get_attribute(cell_value, LAND_ATTR_EXISTS) or
                cell_get_attribute(cell_value, LAND_ATTR_BLOCKED) or
                !cell_get_attribute(cell_value, LAND_ATTR_PROTECTED))
            {
                return false;
            }
        }
    }
    
    return true;
}
