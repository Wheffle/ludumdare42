/// ds_list_replace_value(ds_list, old_value, new_value);
/*
    Replaces the first instance of a value with a new value in
    a list, or does nothing if the value isn't in the list.
*/

var idx = ds_list_find_index(argument0, argument1);
if (idx >= 0)
{
    ds_list_replace(argument0, idx, argument2);
}

