/// shape_set_land(shape_id, attribute);

with(argument0)
{
    for (var i = 0; i < ds_grid_width(grid); i++)
    {
        for (var j = 0; j < ds_grid_height(grid); j++)
        {
            var shape_cell = grid[# i, j];
            if (shape_cell)
            {
                var i_land = cell_x + shape_to_land_offset_x(i, j, rotation);
                var j_land = cell_y + shape_to_land_offset_y(i, j, rotation);
                landmass_set_cell_attribute(land, i_land, j_land, argument1);
            }
        }
    }
}
