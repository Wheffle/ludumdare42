/// building_set_powered(bulding_id, powered);

with(argument0)
{
    powered = argument1;
    if (powered) 
    {
        sprite_index = sprite_powered;
        image_blend = powered_blend;
    }
    else 
    {
        sprite_index = sprite_unpowered;
        image_blend = unpowered_blend;
    }
}
