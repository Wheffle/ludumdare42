/// landmass_recalculate_protected_areas(landmass_id);

with(argument0)
{
    landmass_clear_attribute(id, LAND_ATTR_PROTECTED);
    landmass_clear_attribute(id, LAND_ATTR_TESTED);
    
    var prev_protected = protected_cells;
    protected_cells = 0;
    
    for (var i = 0; i < width; i++)
    {
        for (var j = 0; j < height; j++)
        {
            var cell_value = landmass_get_cell(id, i, j);
            if (cell_get_attribute(cell_value, LAND_ATTR_EXISTS) and
                !cell_get_attribute(cell_value, LAND_ATTR_WALLED) and
                !cell_get_attribute(cell_value, LAND_ATTR_TESTED))
            {
                landmass_fill_if_enclosed(id, i, j, LAND_ATTR_PROTECTED, LAND_ATTR_WALLED);
            }
        }
    }
    
    landmass_refresh_surface(id);
    
    with(parent_building)
    {
        var protected = landmass_get_cell_attribute(land, cell_x, cell_y, LAND_ATTR_PROTECTED);
        building_set_powered(id, protected);
    }
    
    with(obj_tower)
    {
        spell_power_gain = (1 / 120) + (5 / 120)*clamp(other.protected_cells / other.max_cells, 0, 1);
    }
    
    if (protected_cells > prev_protected)
    {
        sfx_play_sound(sfx_powergain);
    }
    else if (protected_cells < prev_protected)
    {
        sfx_play_sound(sfx_powerloss);
    }
}
