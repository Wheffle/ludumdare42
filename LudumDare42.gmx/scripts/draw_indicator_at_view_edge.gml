///draw_indicator_at_view_edge(x_object, y_object, sprite_index)

var xDraw, yDraw;

var xRadius = floor(view_wview[0]*0.5);
var yRadius = floor(view_hview[0]*0.5);
var xCenter = view_xview[0]+xRadius;
var yCenter = view_yview[0]+yRadius;
var dir = point_direction(xCenter, yCenter, argument0, argument1);
var radians = degtorad(dir);
var tangent = tan(radians);
var yy = xRadius * tangent;

if (abs(yy) <= yRadius)
{
  if (radians < pi*0.5 || radians > (pi + pi*0.5)) {
    xDraw = xRadius;
    yDraw = -yy;
  } else {
    xDraw = -xRadius;
    yDraw = yy;
  }
}
else
{
  var xx = yRadius / tangent;
  if (radians < pi) {
    xDraw = xx;
    yDraw = -yRadius;
  } else {
    xDraw = -xx;
    yDraw = yRadius;
  }
}

draw_sprite(argument2, 0, xCenter+xDraw, yCenter+yDraw);
