/// landmass_redraw_surface(landmass_id);

with(argument0)
{
    if (surface_exists(surface)) surface_free(surface);
    surface = surface_create(width*CELL_W, height*CELL_H + CELL_H);
    
    surface_set_target(surface);
    
    draw_clear_alpha(c_black, 0);
    
    for (var i = 0; i < width; i++)
    {
        for (var j = 0; j < height; j++)
        {
            var cell_status = grid[# i, j];
            var xx = i*CELL_W;
            var yy = j*CELL_H;
            
            if (cell_get_attribute(cell_status, LAND_ATTR_EXISTS))
            {
                draw_sprite(s_land, 0, xx, yy);
            }
            
            if (cell_get_attribute(cell_status, LAND_ATTR_PROTECTED))
            {
                draw_set_colour_write_enable(true, true, true, false);
                draw_sprite_ext(s_highlight, 0, xx, yy, 1, 1, 0, c_blue, 0.25);
                draw_set_colour_write_enable(true, true, true, true);
            }
        }
    }   
    
    surface_reset_target();
}
