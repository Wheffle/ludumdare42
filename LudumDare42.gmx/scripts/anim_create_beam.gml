/// anim_id = anim_create_beam(x1, y1, x2, y2, sprite, image_speed, image_blend);

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var sprite = argument4;
var img_speed = argument5;
var img_blend = argument6;

var anim = instance_create(x1, y1, obj_anim);
with(anim)
{
    
    sprite_index = sprite;
    image_speed = img_speed;
    image_blend = img_blend;
    
    image_angle = point_direction(x1, y1, x2, y2);
    image_xscale = point_distance(x1, y1, x2, y2) / sprite_get_width(sprite);
}
return anim;
