/// camera_id = camera_create(x, y, view);

var c = instance_create(0, 0, obj_camera);
with(c) 
{
    view = argument2;
    
    width = view_wview[view];
    height = view_hview[view];
    half_width = width div 2;
    half_height = height div 2;
    
    x = argument0 - half_width;
    y = argument1 - half_height;
    camera_update();
}

return c;
