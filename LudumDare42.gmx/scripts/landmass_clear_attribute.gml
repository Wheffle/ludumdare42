/// landmass_clear_attribute(landmass_id, attr);

with(argument0)
{
    for (var i = 0; i < width; i++)
    {
        for (var j = 0; j < height; j++)
        {
            landmass_clear_cell_attribute(id, i, j, argument1);
        }
    }
}
