/// key = ds_map_find_key(ds_map, value);

var key = ds_map_find_first(argument0);
while(!is_undefined(key))
{
    if (ds_map_find_value(argument0, key) == argument1) return key;
    key = ds_map_find_next(argument0, key);
}
return undefined;

