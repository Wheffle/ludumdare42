/// block_id = landmass_spawn_block(landmass_id, cell_x, cell_y, block_type);

with(argument0)
{
    var block_x = x + argument1*CELL_W;
    var block_y = y + argument2*CELL_H;
    var block = instance_create(block_x, block_y, argument3);
    
    
    
    var wobble = 8;
    var anim_x = block.x_center - wobble + irandom(wobble*2);
    var anim_y = block.y_center - wobble + irandom(wobble*2);
    anim_create(anim_x, anim_y, s_smoke, 10 / room_speed, c_white, 0, block.sprite_width / 32);
    
    camera_shake(0, 5);
    
    return block;
}

show_debug_message("ERROR: landmass_spawn_block() called with invalid landmass id");
return undefined;
