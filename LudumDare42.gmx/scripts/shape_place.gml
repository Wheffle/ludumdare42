/// shape_place(shape_id);

with(argument0)
{
    for (var i = 0; i < ds_grid_width(grid); i++)
    {
        for (var j = 0; j < ds_grid_height(grid); j++)
        {
            var shape_cell = grid[# i, j];
            if (shape_cell)
            {
                var i_land = cell_x + shape_to_land_offset_x(i, j, rotation);
                var j_land = cell_y + shape_to_land_offset_y(i, j, rotation);
                var w = landmass_spawn_block(land, i_land, j_land, obj_wall);
                
                with(obj_enemy_soldier)
                {
                    if (position_meeting(x, y, w))
                    {
                        damage_enemy(id, 30);
                    }
                }
            }
        }
    }
    
    landmass_recalculate_protected_areas(land);
    instance_destroy();
}
