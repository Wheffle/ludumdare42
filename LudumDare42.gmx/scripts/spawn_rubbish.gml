/// rubbish_id = spawn_rubbish(x, y, shape_type);

var r = instance_create(argument0, argument1, obj_rubbish);
r.shape = argument2;
r.sprite_index = object_get_sprite(argument2);
return r;
