/// sfx_play_loop(sound_id);

if (!audio_is_playing(argument0))
{
    audio_play_sound(argument0, 10, false);
}
