/// tower_id = place_tower(landmass_id, border_width);

with(argument0)
{
    var border = argument1;
    
    var cell_x = obj_marker_tower.cell_x;
    var cell_y = obj_marker_tower.cell_y;
    
    // build walls and make sure land around tower exists
    var x1 = cell_x - 1 - border;
    var y1 = cell_y - 1 - border;
    var x2 = cell_x + 2 + border;
    var y2 = cell_y + 2 + border;
    
    for (var i = x1; i <= x2; i++)
    {
        for (var j = y1; j <= y2; j++)
        {
            landmass_set_cell_attribute(id, i, j, LAND_ATTR_EXISTS);
            if (i == x1 or i == x2 or j == y1 or j == y2)
            {
                landmass_spawn_block(id, i, j, obj_wall);
            }
        }
    }
    
    return landmass_spawn_block(id, cell_x, cell_y, obj_tower);
}

show_debug_message("ERROR: place_tower() called with invalid landmass id");
return undefined;
