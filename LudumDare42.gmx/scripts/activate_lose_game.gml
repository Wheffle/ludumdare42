/// activate_lose_game();

with(ctrl_game)
{
    show_popup_message("Maybe the flying island was a bad idea...", room_speed*10);
    alarm[1] = room_speed*10;
}
