/// bool = shape_can_place(shape_id);

with(argument0)
{
    for (var i = 0; i < ds_grid_width(grid); i++)
    {
        for (var j = 0; j < ds_grid_height(grid); j++)
        {
            var shape_cell = grid[# i, j];
            
            if (!shape_cell) 
            {
                continue;
            }
        
            var i_land = cell_x + shape_to_land_offset_x(i, j, rotation);
            var j_land = cell_y + shape_to_land_offset_y(i, j, rotation);
            var land_cell = landmass_get_cell(land, i_land, j_land);
            
            if (is_undefined(land_cell) or 
                !cell_get_attribute(land_cell, LAND_ATTR_EXISTS) or
                cell_get_attribute(land_cell, LAND_ATTR_BLOCKED))
            {
                return false;
            }
        }
    }
    
    return true;
}

show_debug_message("ERROR: shape_can_place() called with invalid shape id");
return undefined;
