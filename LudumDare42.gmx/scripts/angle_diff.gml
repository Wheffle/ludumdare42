/// degrees = angle_diff(angle1, angle2);

var diff = abs(argument1 - argument0);
if (diff > 180) diff = abs(diff - 360);
return diff;

