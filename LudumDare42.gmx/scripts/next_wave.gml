/// next_wave();

with(ctrl_game)
{
    if (wave == last_wave) exit;
    
    wave++;
    
    if (wave != last_wave)
    {
        wave_timer_seconds = 75;
        wave_timer = room_speed;
    }
    
    switch(wave)
    {
        case 1:
            repeat(2) ds_queue_enqueue(spawn_queue, obj_enemy_boat_easy);
            show_popup_message("More pirates incoming! Take them out and continue building up enclosures.", room_speed*15);
            break;
        
        case 2:
            repeat(2) ds_queue_enqueue(spawn_queue, obj_enemy_boat_easy);
            repeat(1) ds_queue_enqueue(spawn_queue, obj_enemy_boat);
            show_popup_message("Flying is not this island's intended use... it's taking at toll!", room_speed*10);
            camera_shake(30, 30);
            landmass_break(land, 50);
            break;
            
        case 3:
            repeat(4) ds_queue_enqueue(spawn_queue, obj_enemy_boat);
            camera_shake(30, 30);
            landmass_break(land, 50);
            break;
            
        case 4:
            repeat(4) ds_queue_enqueue(spawn_queue, obj_enemy_boat);
            repeat(2) ds_queue_enqueue(spawn_queue, obj_enemy_boat_hard);
            repeat(1) ds_queue_enqueue(spawn_queue, obj_enemy_eater);
            camera_shake(30, 30);
            landmass_break(land, 50);
            
            break;
            
        case 5:
            repeat(4) ds_queue_enqueue(spawn_queue, obj_enemy_boat);
            repeat(3) ds_queue_enqueue(spawn_queue, obj_enemy_boat_hard);
            repeat(2) ds_queue_enqueue(spawn_queue, obj_enemy_eater);
            camera_shake(30, 30);
            landmass_break(land, 50);
            show_popup_message("This island continues to fall apart! Grab the duct tape!", room_speed*10);
            break;
            
        case 6:
            repeat(5) ds_queue_enqueue(spawn_queue, obj_enemy_boat);
            repeat(4) ds_queue_enqueue(spawn_queue, obj_enemy_boat_hard);
            repeat(3) ds_queue_enqueue(spawn_queue, obj_enemy_eater);
            camera_shake(30, 30);
            landmass_break(land, 50);
            break;
            
        case 7:
            repeat(6) ds_queue_enqueue(spawn_queue, obj_enemy_boat);
            repeat(6) ds_queue_enqueue(spawn_queue, obj_enemy_boat_hard);
            repeat(3) ds_queue_enqueue(spawn_queue, obj_enemy_eater);
            repeat(1) ds_queue_enqueue(spawn_queue, obj_enemy_boss);
            show_popup_message("Incoming pirate flagship! They won't be back if they lose the pride of their navy.", room_speed*10);
            break;
    }
}
