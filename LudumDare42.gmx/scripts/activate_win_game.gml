/// activate_win_game();

with(ctrl_game)
{
    if (flagship_down)
    {
        show_popup_message("A new world is on the horizon! Time to park this ride...", room_speed*10);
        alarm[0] = room_speed*10;
    }
}
