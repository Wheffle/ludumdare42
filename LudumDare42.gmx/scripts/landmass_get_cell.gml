/// value = landmass_get_cell(landmass_id, x, y);

with(argument0)
{
    var cell_x = argument1;
    var cell_y = argument2;
    
    if (cell_x < 0 or cell_x >= width or cell_y < 0 or cell_y >= height)
        return undefined;
        
    return grid[# cell_x, cell_y];
}

show_debug_message("ERROR: landmass_get_cell() called with invalid landmass id");
return undefined;
