/// write_to_file(string, filename);

var file = file_text_open_write(working_directory + argument1);
if (file >= 0)
{
    file_text_write_string(file, argument0);
    file_text_close(file);
}
else
{
    show_debug_message("There was a problem writing to " + argument1);
}

