/// enemy_id = enemy_spawn(enemy_type);

var xx = 0;
var yy = 0;
var spawn_side = choose("north", "south", "east", "west");

switch(spawn_side)
{
    case "north":
        xx = irandom(room_width);
        yy = -16;
        break;
    
    case "south":
        xx = irandom(room_width);
        yy = room_height + 16;
        break;
    
    case "east":
        xx = room_width+16;
        yy = irandom(room_height);
        break;
    
    case "west":
        xx = -16;
        yy = irandom(room_height);
        break;
}

return instance_create(xx, yy, argument0);
