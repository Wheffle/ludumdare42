/// sfx_play_sound(sound_id);

if (audio_is_playing(argument0)) audio_stop_sound(argument0);
audio_play_sound(argument0, 10, false);
