/// place_crystals();

var land = get_landmass();

var marker_list = ds_list_create();
var crystal_list = ds_list_create();

ds_list_add(crystal_list, obj_crystal_red, obj_crystal_green);
ds_list_shuffle(crystal_list);

with(obj_marker_crystal)
{
    ds_list_add(marker_list, id);
}

ds_list_shuffle(marker_list);

for (var i = 0; i < ds_list_size(marker_list) and i < ds_list_size(crystal_list); i++)
{
    var marker = marker_list[| i];
    var crystal = crystal_list[| i];
    landmass_spawn_block(land, marker.cell_x, marker.cell_y, crystal);
}

ds_list_destroy(marker_list);
ds_list_destroy(crystal_list);
