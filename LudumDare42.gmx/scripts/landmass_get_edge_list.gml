/// list = landmass_get_edge_list(landmass_id);

with(argument0)
{
    var list = ds_list_create();
    
    for (var i = 0; i < width; i++)
    {
        for (var j = 0; j < height; j++)
        {
            var cell_value = landmass_get_cell(id, i, j);
            if (cell_get_attribute(cell_value, LAND_ATTR_EXISTS) and
                cell_is_edge(id, i, j))
            {
                var cell_map = ds_map_create();
                cell_map[? "x"] = i;
                cell_map[? "y"] = j;
                ds_list_add(list, cell_map);  
            }
        }
    }
    
    return list;
}

return undefined;
