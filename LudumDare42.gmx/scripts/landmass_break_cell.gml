/// landmass_break_cell(landmass_id, x, y);

var cell_x = argument1;
var cell_y = argument2;

with(argument0)
{
    grid[# cell_x, cell_y] = 0;
    
    var block = collision_point(x+cell_x*CELL_W+(CELL_W div 2), y+cell_y*CELL_H+(CELL_H div 2), parent_block, true, true);
    if (block) damage_block(block, 10000, block.x_center, block.y_center);

    var xx = x + (cell_x*CELL_W);
    var yy = y + (cell_y*CELL_H);   
    
    var xx_center = xx + (CELL_W div 2);
    var yy_center = yy + (CELL_H div 2);
    var wobble = 4;
    
    repeat(3)
    {
        anim_create(xx_center-wobble+irandom(wobble*2), yy_center-wobble+irandom(wobble*2), s_moon, 10 / room_speed, c_olive, irandom(360), 0.25 + random(0.25));
    }
    
    anim_create(xx_center, yy_center, s_smoke, 10 / room_speed, c_white, 0, 0.5);
    
    
    
    var anim = anim_create(xx, yy, s_land_falling, 3 / room_speed, c_white, 0, 1);
    anim.vspeed = 0.2;
    anim.depth = 101;
}
