/// land_id = get_landmass();

with(ctrl_game)
{
    return land;
}

show_debug_message("ERROR: get_landmass() called but no ctrl_game exists");
return undefined;
