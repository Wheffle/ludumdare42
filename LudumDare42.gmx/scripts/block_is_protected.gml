/// block_is_protected(bock_id);

with(argument0)
{
    return false;

    if (object_index == obj_wall) 
    {
        return false;
    }
    
    return powered;
}

show_debug_message("ERROR: block_is_protected() called on an invalid block id");
return undefined;
