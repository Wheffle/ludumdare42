/// landmass_clear_cell_attribute(landmass_id, x, y, attr);

with(argument0)
{
    var cell_x = argument1;
    var cell_y = argument2;
    var attr = argument3;
    
    if (cell_x < 0 or cell_x >= width or cell_y < 0 or cell_y >= height)
        exit;
        
    var previous_value = grid[# cell_x, cell_y];
    grid[# cell_x, cell_y] = previous_value & (~attr);
}
