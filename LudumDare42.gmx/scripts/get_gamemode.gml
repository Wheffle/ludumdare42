/// gamemode = get_gamemode();

with(ctrl_game)
{
    return mode;
}

show_debug_message("ERROR: get_gamemode() called but no ctrl_game exists");
return undefined;
