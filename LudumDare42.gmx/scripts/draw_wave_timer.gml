/// draw_wave_timer(x, y, wave_number, time);

var width = 64;
var height = 38;

var x1 = argument0;
var y1 = argument1;
var x2 = x1+width;
var y2 = y1+height;

var text1_x = x1 + width div 2;
var text1_y = y1 + 12;

var text2_x = x1 + width div 2;
var text2_y = y1 + 26;

var wave_num = argument2;
var time = argument3;

draw_set_color(c_white);
draw_roundrect(x1, y1, x2, y2, false);

draw_set_color(c_black);
draw_roundrect(x1+2, y1+2, x2-2, y2-2, false);

draw_set_font(font0);
draw_set_halign(fa_center);
draw_set_valign(fa_center);

draw_set_color(c_white);
draw_text(text1_x, text1_y, "Wave " + string(wave_num));
draw_text(text2_x, text2_y, string(time));

draw_set_halign(fa_left);
draw_set_valign(fa_top);
