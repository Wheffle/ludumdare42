/// show_popup_message(message, time);

with(ctrl_game)
{
    message = argument0;
    message_timer = argument1;
    sfx_play_sound(sfx_message);
}
