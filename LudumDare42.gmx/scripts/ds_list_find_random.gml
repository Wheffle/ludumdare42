/// value = ds_list_find_random(ds_list_index);

if (ds_list_size(argument0) == 0)
{
    return undefined;
}

var index = irandom(ds_list_size(argument0)-1);
return ds_list_find_value(argument0, index);

