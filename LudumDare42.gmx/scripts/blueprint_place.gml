/// blueprint_place(blueprint_id);

with(argument0)
{
    landmass_spawn_block(land, cell_x, cell_y, building_type);
    instance_destroy();
}
