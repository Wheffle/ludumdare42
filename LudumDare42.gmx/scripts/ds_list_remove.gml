///ds_list_remove(id, value);
/*
    Removes an instance of value from the list. If none exist, does nothing.
*/

var pos = ds_list_find_index(argument0, argument1);
if (pos >= 0) ds_list_delete(argument0, pos);



