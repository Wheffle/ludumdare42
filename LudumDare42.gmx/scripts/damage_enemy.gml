/// damage_enemy(enemy_id, damage);

with(argument0)
{
    hp -= argument1;
    
    var hurt_num = ceil(argument1 / 5);
    var hurt_wobble_x = sprite_width div 2;
    var hurt_wobble_y = sprite_height div 2;
    repeat(hurt_num)
    {
        var anim_x = x-hurt_wobble_x+irandom(hurt_wobble_x*2);
        var anim_y = y-hurt_wobble_y+irandom(hurt_wobble_y*2);
        var anim = anim_create(anim_x, anim_y, s_hurt, 15 / room_speed, hurt_blend, random(360), hurt_size);
        anim.hspeed = -0.25 + random(0.5);
        anim.vspeed = -0.25 + random(0.5);
    }
    
    if (hp <= 0)
    {
        camera_shake(2, 2);
        
        if (is_last_wave() and instance_number(parent_enemy) == 1)
        {
            activate_win_game();
        }
        
        instance_destroy();
    }
}
