/// landmass_refresh_surface(landmass_id);

with(argument0)
{
    if (surface_exists(surface)) surface_free(surface);
}
