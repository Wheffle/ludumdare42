/// camera_snap_to(x, y);

with(obj_camera)
{
    x = argument0 - half_width;
    y = argument1 - half_height;
    camera_update();
}
