/// landmass_break(landmass_id, tiles_to_break);

with(argument0)
{
    // Get list of edge peices
    var list = landmass_get_edge_list(id);
    
    // Choose some to destroy
    ds_list_shuffle(list);
    for (var i = 0; i < ds_list_size(list) and i < argument1; i++)
    {
        var map = list[| i];
        var c_x = map[? "x"];
        var c_y = map[? "y"];
        landmass_break_cell(id, c_x, c_y);
    }
    
    landmass_refresh_surface(id);
    landmass_recalculate_protected_areas(id);
    
    // Cleanup
    edge_list_destroy(list);
    
    sfx_play_sound(sfx_earthquake);
}
