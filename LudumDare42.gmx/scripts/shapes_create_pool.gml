/// ds_list = shapes_create_pool();

var list = ds_list_create();
ds_list_add(list, 
    obj_shape_line, 
    obj_shape_el, 
    obj_shape_diag,
    obj_shape_zig,
    obj_shape_tee,
    obj_shape_plus);
return list;
