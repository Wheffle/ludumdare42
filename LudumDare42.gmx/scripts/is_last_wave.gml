/// bool = is_last_wave();

with(ctrl_game)
{
    return (wave == last_wave);
}

return undefined;
