/// bool = landmass_cell_in_bounds(landmass_id, cell_x, cell_y);

with(argument0)
{
    var xx = argument1;
    var yy = argument2;
    
    return (xx >= 0 and xx < width and yy >= 0 and yy < height);
}

show_debug_message("ERROR: landmass_cell_in_bounds() called on an invalid landmass id");
return undefined;
