/// blueprint = blueprint_create(x, y, building_type);

var b = instance_create(argument0, argument1, obj_blueprint);
with(b)
{
    building_type = argument2;
    sprite_index = object_get_sprite(building_type);
}
return b;
