/// draw_beam(x1, y1, x2, y2, sprite, image_blend);

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;

var sprite = argument4;
var blend = argument5;

var angle = point_direction(x1, y1, x2, y2);
var xscale = point_distance(x1, y1, x2, y2) / sprite_get_width(sprite);

draw_sprite_ext(sprite, 0, x1, y1, xscale, 1, angle, blend, 1);
