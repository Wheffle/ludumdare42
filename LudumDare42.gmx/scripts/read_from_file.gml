/// string = read_from_file(filename);

if (file_exists(working_directory + argument0))
{
    var file = file_text_open_read(working_directory + argument0);
    if (file >= 0)
    {
        var text = "";
        
        while(!file_text_eof(file))
        {
            text += file_text_readln(file);
        }
        
        file_text_close(file);
        
        return text;
    }
    else
    {
        show_debug_message("Error reading from file: " + argument0);
    }
}
else
{
    show_debug_message("Unable to read from file, doesn't exist: " + argument0);
}

return "";

