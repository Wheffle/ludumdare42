/// landmass_cell_enclosed(landmass_id, cell_x, cell_y, blocking_attr);

with(argument0)
{
    landmass_clear_attribute(id, LAND_ATTR_TESTED);

    var start_x = argument1;
    var start_y = argument2;
    var blocking_attr = argument3;

    var enclosed = true;
    
    var stack = ds_stack_create();
    ds_stack_push(stack, start_y);
    ds_stack_push(stack, start_x);
    
    while(ds_stack_size(stack) > 0)
    {
        var xx = ds_stack_pop(stack);
        var yy = ds_stack_pop(stack);
        var cell_value = landmass_get_cell(id, xx, yy);
        
        if (is_undefined(cell_value) or
            !cell_get_attribute(cell_value, LAND_ATTR_EXISTS))
        {
            enclosed = false;
            break;
        } 
        
        if (!cell_get_attribute(cell_value, LAND_ATTR_TESTED) and
            !cell_get_attribute(cell_value, blocking_attr))
        {
            for (var i = -1; i < 2; i++)
            {
                for (var j = -1; j < 2; j++)
                {
                    if (i != 0 or j != 0)
                    {
                        ds_stack_push(stack, yy+j);
                        ds_stack_push(stack, xx+i);
                    }
                }
            }
        }
        
        landmass_set_cell_attribute(id, xx, yy, LAND_ATTR_TESTED);
    }

    if (enclosed)
    {
        ds_stack_destroy(stack);
        return true;
    }
    else
    {
        ds_stack_destroy(stack);
        return false;
    }
    
    ds_stack_destroy(stack);
    return enclosed;
}

show_debug_message("ERROR: landmass_cell_open_to_edge() called with invalid landmass id");
return undefined;
