/// bool = ds_priority_exists(ds_priority, value);

var val = ds_priority_find_priority(argument0, argument1);
return (!is_undefined(val));

