/// edge_list_destroy(list);

var list = argument0;

for (var i = 0; i < ds_list_size(list); i++)
{
    var map = list[| i];
    ds_map_destroy(map);
}
ds_list_destroy(list);
