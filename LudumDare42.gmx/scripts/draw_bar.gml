/// draw_bar(x, y, width, height, color, pct, label);

var xx = argument0;
var yy = argument1;
var width = argument2;
var height = argument3;
var color = argument4;
var pct = argument5;
var label = argument6;

var half_width = width div 2;
var half_height = height div 2;

var x1 = xx - half_width;
var y1 = yy;
var x2 = xx + half_width;
var y2 = yy + height;

draw_set_color(c_white);
draw_roundrect(x1, y1, x2, y2, false);

draw_set_color(c_black);
draw_roundrect(x1+2, y1+2, x2-2, y2-2, false);

var bar_width = pct * ((x2-2) - (x1+2));
draw_set_color(color);
draw_roundrect(x1+2, y1+2, x1+2+bar_width, y2-2, false);

draw_set_font(font0);
draw_set_color(c_white);
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_text(x1 + half_width, y1 + half_height, label);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
