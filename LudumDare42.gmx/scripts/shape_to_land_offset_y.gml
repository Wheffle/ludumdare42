/// real = shape_to_land_offset_y(i, j, rotation);

return lengthdir_x(argument1, argument2) + lengthdir_y(argument0, argument2);
