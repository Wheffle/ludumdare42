/// anim_id = anim_create(x, y, sprite, image_speed, image_blend, image_angle, image_scale);

var anim = instance_create(argument0, argument1, obj_anim);
with(anim)
{
    sprite_index = argument2;
    image_speed = argument3;
    image_blend = argument4;
    image_angle = argument5;
    
    image_xscale = argument6;
    image_yscale = argument6;
}
return anim;
