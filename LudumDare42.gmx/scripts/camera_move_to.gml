/// camera_move_to(x, y);

with(obj_camera)
{
    move_x = argument0;
    move_y = argument1;
    moving = true;
}
