/// draw_power(x, y, amount);

var width = 64;
var height = 38;

var text1_x = width div 2;
var text1_y = 12;

var text2_x = width div 2;
var text2_y = 26;

var x1 = argument0;
var y1 = argument1;
var x2 = x1+width;
var y2 = y1+height;

var amount = argument2;

draw_set_color(c_white);
draw_roundrect(x1, y1, x2, y2, false);

draw_set_color(c_black);
draw_roundrect(x1+2, y1+2, x2-2, y2-2, false);

draw_set_font(font0);
draw_set_halign(fa_center);
draw_set_valign(fa_center);

draw_set_color(c_white);
draw_text(text1_x, text1_y, "Power:");

draw_set_color(c_yellow)
draw_text(text2_x, text2_y, string(amount));

draw_set_halign(fa_left);
draw_set_valign(fa_top);
