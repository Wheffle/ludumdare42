/// landmass_fill_if_enclosed(landmass_id, start_x, start_y, fill_attr, blocking_attr);

with(argument0)
{
    var start_x = argument1;
    var start_y = argument2;
    var fill_attr = argument3;
    var blocking_attr = argument4;
    
    var enclosed = true;
    
    var area_stack = ds_stack_create();
    
    var stack = ds_stack_create();
    ds_stack_push(stack, start_y);
    ds_stack_push(stack, start_x);
    
    while(ds_stack_size(stack) > 0)
    {
        var xx = ds_stack_pop(stack);
        var yy = ds_stack_pop(stack);
        var cell_value = landmass_get_cell(id, xx, yy);
        
        if (cell_get_attribute(cell_value, LAND_ATTR_TESTED))
        {
            continue;
        }
        
        if (is_undefined(cell_value) or !cell_get_attribute(cell_value, LAND_ATTR_EXISTS))
        {
            enclosed = false;
        }
        else if (!cell_get_attribute(cell_value, blocking_attr))
        {
            ds_stack_push(area_stack, yy, xx);
            
            for (var i = -1; i < 2; i++)
            {
                for (var j = -1; j < 2; j++)
                {
                    if (i != 0 or j != 0)
                    {
                        ds_stack_push(stack, yy+j);
                        ds_stack_push(stack, xx+i);
                    }
                }
            }
        }
        
        landmass_set_cell_attribute(id, xx, yy, LAND_ATTR_TESTED);
    }
    
    if (enclosed)
    {
        while(ds_stack_size(area_stack) > 0)
        {
            var xx = ds_stack_pop(area_stack);
            var yy = ds_stack_pop(area_stack);
            landmass_set_cell_attribute(id, xx, yy, fill_attr);
            protected_cells++;
        }
    }
    
    ds_stack_destroy(stack);
    ds_stack_destroy(area_stack);
}
