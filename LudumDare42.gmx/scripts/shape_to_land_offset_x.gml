/// real = shape_to_land_offset_x(i, j, rotation);

return lengthdir_x(argument0, argument2) - lengthdir_y(argument1, argument2);
