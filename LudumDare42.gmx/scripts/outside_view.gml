/// bool = outside_view(x, y);

var xx = argument0;
var yy = argument1;

if (xx < view_xview[0] or xx > view_xview[0] + view_wview[0] or
    yy < view_yview[0] or yy > view_yview[0] + view_hview[0])
{
    return true;    
}

return false;
