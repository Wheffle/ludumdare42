/// landmass_id = init_landmass();

var width = (room_width+1) div CELL_W
var height = (room_height+1) div CELL_H
var land = landmass_create(0, 0, width, height);

with(parent_marker)
{
    cell_x = (land.x + x) div CELL_W;
    cell_y = (land.y + y) div CELL_H;
    landmass_set_cell_attribute(land, cell_x, cell_y, LAND_ATTR_EXISTS);
}

with(obj_marker_land) instance_destroy();

return land;

