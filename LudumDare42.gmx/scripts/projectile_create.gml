/// proj_id = projectile_create(x, y, target_x, target_y, projectile_type);

var p = instance_create(argument0, argument1, argument4);
with(p)
{
    move_x = argument2;
    move_y = argument3;
}
return p;
