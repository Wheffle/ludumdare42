/// landmass_get_cell_x(landmass_id, room_x);

with(argument0)
{
    return (argument1 - x) div CELL_W;
}

return undefined;
