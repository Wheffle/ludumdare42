/// damage_block(block_id, damage, anim_x, anim_y);

with(argument0)
{
    if (block_is_protected(id))
    {   
        anim_create(argument2, argument3, s_shield, 10 / room_speed, c_blue, 0, 1);
    }
    else
    {
        hp -= argument1;
        if (argument1 >= 5) camera_shake(2, 2);
        if (hp <= 0)
        {
            var smokes = 1;
            if (sprite_width > 16) smokes += 2;
            repeat(smokes)
            {
                anim_create(x+irandom(sprite_width), y+irandom(sprite_height), s_moon,
                    5 / room_speed, c_black, random(360), sprite_width / 32);
            }
            
            camera_shake(10, 10);
            sfx_play_sound(sfx_collapse);
            instance_destroy();
        }
    }
}
