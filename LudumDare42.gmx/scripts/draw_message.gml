/// draw_message(x, y, max_width, text);

var xx = argument0;
var yy = argument1;
var max_width = argument2-8;
var text = argument3;

draw_set_font(font0);
var width = 8 + string_width_ext(text, -1, max_width);
var height = 8 + string_height_ext(text, -1, max_width);

var x1 = xx-(width div 2);
var y1 = yy;
var x2 = xx+(width div 2);
var y2 = yy+height;

draw_set_alpha(0.5);

//draw_set_color(c_yellow);
//draw_roundrect(x1, y1, x2, y2, false);

draw_set_color(c_black);
draw_roundrect(x1, y1, x2, y2, false);

draw_set_alpha(1);

draw_set_color(c_yellow);
draw_set_halign(fa_center);
draw_text_ext(xx, yy+4, text, -1, max_width);

draw_set_halign(fa_left);


