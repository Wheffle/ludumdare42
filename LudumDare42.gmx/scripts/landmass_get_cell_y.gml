/// landmass_get_cell_y(landmass_id, room_y);

with(argument0)
{
    return (argument1 - y) div CELL_H;
}

return undefined;
