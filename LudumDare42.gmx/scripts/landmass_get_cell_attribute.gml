/// value = landmass_get_cell_attribute(landmass_id, x, y, attr);

with(argument0)
{
    var cell_x = argument1;
    var cell_y = argument2;
    var attr = argument3;
    
    if (cell_x < 0 or cell_x >= width or cell_y < 0 or cell_y >= height)
        return undefined;
        
    var value = grid[# cell_x, cell_y];
    return cell_get_attribute(value, attr);
}

show_debug_message("ERROR: landmass_get_cell_attribute() called with invalid landmass id");
return undefined;
