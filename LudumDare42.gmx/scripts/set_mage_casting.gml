/// set_mage_casting(steps, effect_blend);

with(obj_tower)
{
    mage_sprite = mage_sprite_casting;
    mage_cast_timer = argument0;
    
    anim_create(x+mage_x, y+mage_y, s_rim, 15 / room_speed, argument1, 0, 1);
}
