/// camera_shake(shake_x, shake_y);

with(obj_camera)
{
    shake_x = argument0;
    shake_y = argument1;
}
