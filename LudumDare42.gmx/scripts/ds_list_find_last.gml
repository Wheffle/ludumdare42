/// item = ds_list_find_last(list_id);

var size = ds_list_size(argument0);

if (size == 0) return undefined;

return ds_list_find_value(argument0, size-1);

